# Proposal
Webrings seem to be entirely centralized. This can be problematic in that a central authority controls who is and is not allowed to add, remove, and modify links. Decentralized webrings, like the Lainchan webring, *do* exist but they can be difficult to maintain. I propose a solution involving cron jobs and shell scripts to automate the process of maintaining lists. Every website in the ring will be a *remote host* for the webring that stores and serves a single flat file index of the other websites (probably .csv or .json). This cron job will run nightly, query the index from all other hosts in the ring, then concatenate the files into a single mega index. This index will be de duplicated using a standard `sort | uniq` pipeline. 

Joining the webring is fairly simple: you request that someone already inside of the webring adds you. This provides two main benefits: establishes some level of trust and automatically updates the index on all other servers in the system when the cron job runs. Any user who wishes to join should also be hosing a copy of this index on their own website and running a compatible updater script. This also adds the possibility to add an "is this server up?" field to any custom indexes. 

Cron jobs are preferable because they reduce the request frequency on the "network". cron(8) is the easiest way to accomplish something like this if httpd(8) is running inside of a jail, container, or chroot. Of course, dynamically regenerating this index is entirely possible but will become increasingly slow as the webring increases in size. Additionally, ddos protections might kick in and break any dynamic scripts. 

Other users in the webring are not to be trusted so client developers should take extra care to write scripts and programs that are immune to shell injections and escapes. The cron job should be run as an unprivileged user who writes to the "live index" inside of httpd's document root. This is fairly easy to accomplish using discretionary access control. 

# Joining

In order to join the dring you can either:

1. find an existing user in the dring. He will need to add your website to his own copy of the dring index. 
2. submit a merge request or issue on this repository and I will add your url to my copy of the index

After submitting a request, you should proceed to hosting your own copy of the dring index. Running your own copy of the dring index enables you to easily serve the full index as well as add other websites to the ring. 

# Hosting

You will need to create a daemon user and get a bootstrap copy of the dring index. You can obtain a bootstrap copy with the source code to make it work by cloning this repository. Alternative, you can obtain a bootstrap copy from any member of the dring and implement your own server side software. 

```sh
openbsd# mkdir -p /opt/_dring
openbsd# git clone https://gitlab.com/binrc/dring.git /opt/_dring/
openbsd# adduser -home /opt 
Use option ``-silent'' if you don't want to see all warnings and questions.

Reading /etc/shells
Check /etc/master.passwd
Check /etc/group

Ok, let's go.
Don't worry about mistakes. There will be a chance later to correct any input.
Enter username []: _dring
Enter full name []: dring cron runner
Enter shell bash csh git-shell ksh nologin sh [ksh]: nologin
Uid [1001]: 
Login group _dring [_dring]: 
Login group is ``_dring''. Invite _dring into other groups: guest no 
[no]: 
Login class authpf bgpd daemon default pbuild staff unbound vmd xenodm 
[default]: daemon
Enter password []: 
Disable password logins for the user? (y/n) [n]: y

Name:        _dring
Password:    ****
Fullname:    dring cron runner
Uid:         1001
Gid:         1001 (_dring)
Groups:      _dring 
Login Class: daemon
HOME:        /opt/_dring
Shell:       /sbin/nologin
OK? (y/n) [y]: y
Added user ``_dring''
HOME Directory ``/opt/_dring'' already exists
openbsd# chown -R _dring:_dring /opt/_dring
```

Then, create a cronjob for the `_dring` user. Running dring via a daily cron job is **highly suggested** in order to reduce the total traffic in the dring network. 

```sh
openbsd# crontab -e -u _dring 
openbsd# crontab -l -u _dring
#min    hr      day     month   week
0       0       *       *       *       /opt/_dring/dring.sh
```

Finally, host your copy of the .dring index at <code><i>/docroot</i>/.dring.csv</code>. This is important because it allows other servers to merge your own index into their indices. 

```sh
openbsd# cp /opt/_dring/dot.dring.csv /var/www/htdocs/.dring.csv
openbsd# chown root:_dring /var/www/htdocs/.dring.csv
openbsd# chmod go+w /var/www/htdocs/.dring.csv
```

If you are running PHP, you can use the included `dring.php` file to automatically generate a web page from the index. 

```sh
openbsd# cp /opt/_dring/dring.php /var/www/htdocs/dring.php
```

# Forking

Because of the decentralized nature of dring, it is easily possible to host a "forked index" or "multiple indexes" by renaming `/var/www/htdocs/.dring.csv`

# Trusted Peers
If you don't trust random strangers on the internet, you can easily write a version of the fetch script that only pulls from trusted peers. 

# Leeching

If you do not run your own webserver or use static pages only, you can still join this webring by hosting a static copy of `.dring.csv`. You will have to update this manually from time to time by copying it from a different host. 

# 404 preventions

nothing yet. but eventually. 
