#!/bin/sh
# get link list 
list=$(sed -e 1d -e 's/\"//g' -e 's/,.*/\/.dring.csv/' /var/www/htdocs/.dring.csv);

print $list

for i in $list; do
	curl "$i" | sed 1d >> mega.csv
done;

# deduplicate and generate new list (newlines needed for portability )
sort mega.csv | uniq | sed '1i\
url,title,rss,tor,i2p\
' > .dring.csv

rm mega.csv
cp .dring.csv /var/www/htdocs/.dring.csv
